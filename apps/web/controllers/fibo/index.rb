require 'socket'
module Web::Controllers::Fibo
  class Index
    include Web::Action
    expose :fibo
    expose :host
    expose :processing_time

    def call(params)
      start_time = Time.now
      @fibo = fibonacci(Integer(params.get('number')))
      @host = Socket.gethostname
      end_time = Time.now
      @processing_time = end_time - start_time
    end
    def fibonacci(n)
      return  n  if n <= 1
      fibonacci( n - 1 ) + fibonacci( n - 2 )
    end
  end
end
