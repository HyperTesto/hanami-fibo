FROM ruby:2.3-alpine
MAINTAINER Enrico Testori

RUN apk add --update git \
  build-base \
  libxml2-dev \
  libxslt-dev \
  libgcrypt-dev \
  && rm -rf /var/cache/apk/*

RUN gem install hanami
RUN gem install bundler

RUN mkdir /fibonacci
ADD . /fibonacci
WORKDIR /fibonacci
ENV HANAMI_ENV production
RUN bundle install --deployment --with production
RUN hanami assets precompile
EXPOSE 2300
CMD ["bundle", "exec", "hanami" ,"server"]
