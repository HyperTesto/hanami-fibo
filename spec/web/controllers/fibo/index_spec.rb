require 'spec_helper'
require_relative '../../../../apps/web/controllers/fibo/index'

describe Web::Controllers::Fibo::Index do
  let(:action) { Web::Controllers::Fibo::Index.new }
  let(:params) { Hash[] }

  it 'is successful' do
    response = action.call(params)
    response[0].must_equal 200
  end
end
